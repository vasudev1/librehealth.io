#!/bin/bash

end_epoch=$(date -d "$(echo | openssl s_client -connect librehealth.io:443 -servername librehealth.io 2>/dev/null | openssl x509 -enddate -noout | cut -d'=' -f2)" "+%s")
current_epoch=$(date "+%s")
renew_days_threshold=30
days_diff=$((($end_epoch - $current_epoch) / 60 / 60 / 24))

if [ $days_diff -lt $renew_days_threshold ]; then
  echo "Certificate is $days_diff days old, renewing now."
  acme.sh --issue  -d librehealth.io  --dns dns_gandi_livedns --accountemail infrastructure@librehealth.io --always-force-new-domain-key --days 30
  echo "acme.sh finished. Updating GitLab Pages domains."
  acme.sh --deploy --deploy-hook gitlab -d librehealth.io
else
  echo "Certificate still valid for $days_diff days, no renewal required."
fi
